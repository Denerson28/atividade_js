function bubbleSort(array){
    for(i = 0; i < array.length; i++){
        for(j = i + 1; j < array.length; j++){
            if(array[i] > array[j]){
                let aux = array[i];
                array[i] = array[j];
                array[j] = aux;
            }
        }
    }
}

const tamanho = Math.floor(Math.random() * 101);
console.log(tamanho);
const array = [];

for(i = 0; i < tamanho; i++){
    array.push(Math.floor(Math.random() * 101));
}

bubbleSort(array);

console.log(array);

